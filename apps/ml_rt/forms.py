
import sys, os
from django import forms

here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "../../lib/mercadolibre"))
from meli import Meli


class AddItemForm(forms.Form):

    CONDITIONS_CHOICES = (
        ('new', 'Nuevo'),
        ('used', 'Usado'),
        ('not_specified', 'No especificado')
    )

    BUYING_MODE_CHOICES = (
        ('buy_it_now', 'Comprar ahora'),
   	    ('auction', 'Subasta')
    )
    title = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
       		'name': 'title',
       		'placeholder': 'Ingrese el titulo',
       		'required': 'required'
        }),
        label='Título')

    category_id = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'name': 'category_id',
            'placeholder': 'Ingrese la categoria',
            'required': 'required'
        }),
        label='Categoría', help_text='MLA1103')

    price = forms.DecimalField(
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'name': 'price',
            'placeholder': 'Ingrese el precio',
            'required': 'required'
        }),
        label='Precio', min_value=0.01, decimal_places=2, initial=1)

    available_quantity = forms.IntegerField(
        widget=forms.NumberInput(attrs={
            'class': 'form-control',
            'name': 'available_quantity',
            'placeholder': 'Ingrese la cantidad disponible',
            'required': 'required'
        }),
        label='Cant. disponible', required=False, min_value=1, initial=1)

    buying_mode = forms.ChoiceField(
        widget=forms.Select(attrs={
            'class': 'form-control selector',
            'name': 'buying_mode',
            'required': 'required'
        }),
        label='Seleccione la modalidad de compra', choices=BUYING_MODE_CHOICES)

    listing_type_id = forms.ChoiceField(
        widget=forms.Select(attrs={
            'class': 'form-control selector',
            'name': 'listing_type_id',
            'required': 'required'
        }),
        label='Seleccione el tipo de publicacion')

    condition = forms.ChoiceField(
        widget=forms.Select(attrs={
            'class': 'form-control selector',
            'name': 'condition',
            'required': 'required'
        }),
        label='Seleccione la condición de tu producto', choices=CONDITIONS_CHOICES)

    description = forms.CharField(label='Descripción', widget=forms.Textarea(
        attrs={
            'class': 'form-control',
            'rows': 5, 'cols': 20
            }), required=False)

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        meli = Meli(request.session['CLIENT_ID'], request.session['CLIENT_SECRET'],
                    request.session['ACCESS_TOKEN'], request.session['REFRESH_TOKEN'])
        
        listing_types = list()

        response = meli.get('/sites/MLA/listing_types/')

        for listing_type in response.json():
            listing_types.append((listing_type['id'], listing_type['name']))

        self.fields['listing_type_id'].choices = listing_types

    