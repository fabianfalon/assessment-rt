
import sys, os

from django.conf import settings
from django.shortcuts import redirect
from django.views.generic import FormView, RedirectView, TemplateView
from django.views.generic.list import ListView
from django.urls import reverse
from .forms import AddItemForm

here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "../../lib/mercadolibre"))
from meli import Meli


class LoginView(TemplateView):

    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        client_id = settings.CLIENT_ID
        client_secret = settings.CLIENT_SECRET
        meli = Meli(client_id, client_secret)

        context['redirect_url'] = meli.auth_url(
            redirect_URI=settings.CALLBACK_URL)

        self.request.session['CLIENT_SECRET'] = client_secret
        self.request.session['CLIENT_ID'] = client_id
        return context


class LogoutView(RedirectView):

    def get(self, request, *args, **kwargs):
        self.request.session.flush()
        return super(LogoutView, self).get(request, *args, **kwargs)


class AuthorizeView(RedirectView):

    def get(self, request, *args, **kwargs):
        try:
            code = request.GET['code']
            meli = Meli(
                self.request.session['CLIENT_ID'],
                self.request.session['CLIENT_SECRET']
            )
        except KeyError:
            return redirect('login')

        meli.authorize(
            code=code, redirect_URI=settings.CALLBACK_URL)

        response = meli.get(
            '/users/me', params={'access_token': meli.access_token})

        self.request.session['USER_ID'] = response.json().get('id')
        self.request.session['ACCESS_TOKEN'] = meli.access_token
        self.request.session['REFRESH_TOKEN'] = meli.refresh_token
        return super().get(request, *args, **kwargs)


class ListItemView(TemplateView):

    template_name = 'items.html'

    def get(self, request, *args, **kwargs):
        if self.request.session.get('ACCESS_TOKEN') is None:
            return redirect('login')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['items'] = self.get_items()
        return context

    def get_items(self):
        items_list = list()
        try:
            meli = Meli(client_id=self.request.session['CLIENT_ID'],
                        client_secret=self.request.session['CLIENT_SECRET'],
                        access_token=self.request.session['ACCESS_TOKEN'],
                        refresh_token=self.request.session['REFRESH_TOKEN'])
        except KeyError:
            return redirect('login')

        response = meli.get(
            '/users/{}/items/search'.format(self.request.session['USER_ID']),
            params={'access_token': self.request.session.get('ACCESS_TOKEN')})

        items = response.json().get('results')
        if items:
            for item in items:
                resp = meli.get(f'/items/{item}',
                                params={'access_token': self.request.session.get('ACCESS_TOKEN')})
                resp = resp.json()
                if resp['status'] == 'active':
                    items_list.append({
                        'id': str(item),
                        'permalink': resp['permalink'],
                        'price': resp['price'],
                        'title': resp['title']})
        return items_list


class AddItemView(FormView):

    form_class = AddItemForm
    template_name = 'add_items.html'

    def get(self, request, *args, **kwargs):
        if self.request.session.get('ACCESS_TOKEN') is None:
            return redirect('login')
        return super().get(request, *args, **kwargs)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(**self.get_form_kwargs(), request=self.request)

    def get_success_url(self):
        return reverse('list_items')

    def form_valid(self, form):
        try:
            meli = Meli(client_id=self.request.session['CLIENT_ID'],
                        client_secret=self.request.session['CLIENT_SECRET'],
                        access_token=self.request.session['ACCESS_TOKEN'],
                        refresh_token=self.request.session['REFRESH_TOKEN'])
        except KeyError:
            return redirect('login')

        data = {
            "title": form.cleaned_data['title'],
            "price": str(form.cleaned_data['price']),
            "currency_id": 'ARS',
            "listing_type_id": form.cleaned_data['listing_type_id'],
            "available_quantity": form.cleaned_data['available_quantity'],
            "condition": form.cleaned_data['condition'],
            "description": form.cleaned_data['description'],
            "buying_mode": form.cleaned_data['buying_mode'],
            "category_id": form.cleaned_data['category_id']
        }

        response = meli.post(
            "/items", data,
            {'access_token': self.request.session.get('ACCESS_TOKEN')})
        if response.status_code != 201:
            form.add_error(field=None, error=response.content)
            return super().form_invalid(form)
        return super().form_valid(form)
