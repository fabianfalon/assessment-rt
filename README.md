
### Local Setup

#### 1. Clonar repo:

    $ git clone 
    $ cd assessment-rt
    $ virtualenv -p python3 env
    $ source env/bin/activate    
    $ pip install -r requirements.txt
    $ python manage.py runsslserver --settings=core.settings.local

#### Open [https://127.0.0.1:8000/login](https://127.0.0.1:8000/login)
