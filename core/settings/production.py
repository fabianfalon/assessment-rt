from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Environment variables
CALLBACK_URL = 'CALLBACK_URL'
# podria guardarse como variable de entorno o encriptarse
CLIENT_ID = 'CLIENT_ID'
# podria guardarse como variable de entorno o encriptarse
CLIENT_SECRET = 'CLIENT_SECRET'
