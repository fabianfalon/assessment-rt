from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Environment variables
CALLBACK_URL = 'https://127.0.0.1:8000/authorize'
# podria guardarse como variable de entorno o encriptarse
CLIENT_ID = '2524996558600413'
# podria guardarse como variable de entorno o encriptarse
CLIENT_SECRET = 'CsMjNwhsC2CcLIcvmxfdMfgGZYWaO2cb'
