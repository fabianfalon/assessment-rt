from django.urls import path
from apps.ml_rt.views import LoginView, AuthorizeView, LogoutView, ListItemView, AddItemView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('authorize/', AuthorizeView.as_view(pattern_name='list_items'), name='authorize'),
    path('logout/', LogoutView.as_view(pattern_name='login'), name='logout'),
    path('items/', ListItemView.as_view(), name='list_items'),
    path('items/new', AddItemView.as_view(), name='add_item'),
]
